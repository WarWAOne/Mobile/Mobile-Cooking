package com.warwaone.cooking

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView

import kotlinx.android.synthetic.main.activity_add_plate.*
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.annotation.NonNull
import android.view.View


class AddPlateActivity : AppCompatActivity() {

    private lateinit var mList: RecyclerView
    private lateinit var mAdapter: ProductAdapter
    private lateinit var mBottomSheet: BottomSheetBehavior<ConstraintLayout>
    private lateinit var mLayoutBottomSheet: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_plate)
        setSupportActionBar(toolbar)

        setTitle(R.string.new_plate)

        mList = findViewById(R.id.list)
        mLayoutBottomSheet = findViewById(R.id.bottom_sheet)
        mBottomSheet = BottomSheetBehavior.from(mLayoutBottomSheet)

        val mLayoutManager = LinearLayoutManager(applicationContext)
        mList.layoutManager = mLayoutManager
        mList.itemAnimator = DefaultItemAnimator()
        //mList.addItemDecoration(MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 36))
        mAdapter = ProductAdapter(ProductAdapter.DISPLAY_LINE)
        mList.adapter = mAdapter

        // #TODO: get all product from json table
        //mAdapter.setList()

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            //# TODO: Save plate to DB
        }

        mBottomSheet.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        //btnBottomSheet.setText("Close Sheet")
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        //btnBottomSheet.setText("Expand Sheet")
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }
        })
    }

    fun toggleBottomSheet() {
        if (mBottomSheet.state !== BottomSheetBehavior.STATE_EXPANDED) {
            mBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED)
        } else {
            mBottomSheet.setState(BottomSheetBehavior.STATE_COLLAPSED)
        }
    }
}
