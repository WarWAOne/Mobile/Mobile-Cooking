package com.warwaone.cooking

/**
 * Created by xn1a on 18/02/18.
 */
abstract class Table<Object> {
    val name: String
    var mPath: String = ""

    constructor (name: String) {
        this.name = name
    }

    fun setPath(path: String) {
        mPath = path
    }

    abstract fun add (item: Object): Int
    abstract fun delete (item: Object)
    abstract fun edit (item: Object)
    abstract fun write (json: String)
}