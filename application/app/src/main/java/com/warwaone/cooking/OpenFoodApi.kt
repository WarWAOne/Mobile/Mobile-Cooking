package com.warwaone.cooking

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by xn1a on 23/02/18.
 */

interface OpenFoodApi {
    @GET("/brands.json")
    fun getAllBrands(): Call<List<Any>>

    @GET("/brand/{brand}")
    fun getBrand(@Path("brand") name: String): Call<Any>
}