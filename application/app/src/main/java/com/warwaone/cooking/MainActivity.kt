package com.warwaone.cooking

import android.Manifest
import android.content.Context
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.widget.Toast
import android.content.pm.PackageManager
import android.support.design.widget.BottomNavigationView
import android.support.v4.content.ContextCompat
import android.support.design.widget.FloatingActionButton
import android.view.Menu
import android.net.ConnectivityManager
import android.content.Intent


class MainActivity : AppCompatActivity(), NextMealFragment.OnFragmentInteractionListener,
        ProductFragment.OnFragmentInteractionListener, PlateFragment.OnFragmentInteractionListener {

    private lateinit var mNavigation: BottomNavigationView
    lateinit var fab: FloatingActionButton

    init {
        Database.path = Environment.getExternalStorageDirectory().path+"/"+ DATABASE_PATH
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        mNavigation = findViewById(R.id.navigation)
        fab = findViewById(R.id.fab)

        mNavigation.setOnNavigationItemSelectedListener(handleNavigation)

        val writePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val readPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)

        // Check for permissions
        if (readPermission == PackageManager.PERMISSION_DENIED || writePermission == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        }
        else {
            generateDatabase()
            if (isOnline()) {
                updateProductDatabase()
            }
        }

        title = resources.getString(R.string.title_plates)
        val fragment = NextMealFragment.newInstance()
        showFragment(fragment)
        fab.setImageDrawable(resources.getDrawable(R.drawable.ic_pencil))
    }

    private val handleNavigation = BottomNavigationView.OnNavigationItemSelectedListener { item ->
            val fragment: Fragment
            when (item.itemId) {
                R.id.navigation_next_meal -> {
                    title = resources.getString(R.string.title_next_meal)
                    fragment = NextMealFragment.newInstance()
                    showFragment(fragment)
                    fab.setImageDrawable(resources.getDrawable(R.drawable.ic_pencil))
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_plates -> {
                    title = resources.getString(R.string.title_plates)
                    fragment = PlateFragment.newInstance(this)
                    showFragment(fragment)
                    fab.setImageDrawable(resources.getDrawable(R.drawable.ic_plus))
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_products -> {
                    title = resources.getString(R.string.title_products)
                    fragment = ProductFragment.newInstance()
                    showFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
        false
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    private fun showFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun generateDatabase () {
        Database.createTable(PlateTable(TABLE_PLATE))
        Database.createTable(MealTable(TABLE_MEAL))
        Database.createTable(ProductTable(TABLE_PRODUCT))
    }

    private fun isOnline(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    private fun updateProductDatabase () {
        val intent = Intent(this, ProductDatabaseUpdater::class.java)
        intent.putExtra(ProductDatabaseUpdater.ARG_ACTION, ProductDatabaseUpdater.ACTION_GET_PRODUCTS)
        startService(intent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    generateDatabase()
                    if (isOnline()) {
                        updateProductDatabase()
                    }
                } else {
                    Toast.makeText(this@MainActivity, "Permission denied to read and write " +
                            "your External storage", Toast.LENGTH_SHORT).show()
                    System.exit(0)
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }

    companion object {
        private val DATABASE_PATH = "cooking"

        val TABLE_MEAL = "meal"
        val TABLE_PLATE = "plate"
        val TABLE_PRODUCT = "product"
        /*private val FRAG_DASHBOARD = 0
        private val FRAG_MEAL = 3*/
    }
}
