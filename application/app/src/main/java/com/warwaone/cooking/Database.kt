package com.warwaone.cooking

import android.util.Log
import java.io.File

/**
 * Created by xn1a on 18/02/18.
 */

object Database {
    var path = "cooking"
    private var mTables: MutableList<Table<Any>> = mutableListOf()

    fun createTable (table: Table<Any>) {
        val path: String = path+"/"+table.name+".json"
        val folder = File(path)
        val file = File(path) // #TODO: throw exceptions

        if (!folder.exists()) {
            if (folder.mkdir()) {
                if (file.createNewFile()) {
                    Log.d("SUCCESS", "File created")
                }
                else {
                    Log.d("ERROR", "Erro while creating file")
                }
            }
        }
        else {
            if (file.createNewFile()) {
                Log.d("SUCCESS", "File created")
            }
            else {
                Log.d("ERROR", "Erro while creating file")
            }
        }
        table.setPath(path)
        mTables.add(table)
    }

    fun deleteTable (table: Table<Any>) {
        val file = File(path+"/"+table.name+".json")
        if (file.exists()) {
            file.delete()
        }
    }

    /* Return the table with the given name */
    fun getTable (name: String): Table<Any>? {
        for (table in mTables) {
            if (table.name == name) {
                return table
            }
        }
        return null
    }
}