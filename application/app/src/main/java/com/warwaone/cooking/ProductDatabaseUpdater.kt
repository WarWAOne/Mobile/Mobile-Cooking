package com.warwaone.cooking

import android.app.IntentService
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by xn1a on 22/02/18.
 */
class ProductDatabaseUpdater : IntentService("ProductDatabaseUpdater") {

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl(HTTP_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        val service = retrofit.create<OpenFoodApi>(OpenFoodApi::class.java)
    }

    override fun onHandleIntent(intent: Intent?) {
        //downloadProducts()
    }

    /*class GetProducts : AsyncTask<Void, Int, Boolean>() {
        //var asyncInterface: AsyncResponse? = asyncInterface
        //private var database: Database = database

        /*interface AsyncResponse {
            fun processFinish(output: String)
        }*/

        override fun doInBackground(vararg p0: Void?): Boolean? {
            var productArray = JSONArray()
            //val jsonBrands = AsyncHttpToJson().execute(HTTP_GET_BRANDS).get()
            val jsonBrands =  URL(HTTP_GET_BRANDS).readText()

            val jsonObj = JSONObject(jsonBrands.substring(jsonBrands.indexOf("{"), jsonBrands.lastIndexOf("}") + 1))
            val brands = jsonObj.getJSONArray("tags")

            for (i in 0..brands!!.length() - 1) {
                val name = brands.getJSONObject(i).getString("name")
                val jsonProducts = AsyncHttpToJson().execute(HTTP_GET_BRAND+"/"+name+".json").get()
                //val jsonProducts = URL(HTTP_GET_BRAND+"/"+name+".json").readText()
                val jsonObj = JSONObject(jsonProducts.substring(jsonProducts.indexOf("{"), jsonProducts.lastIndexOf("}") + 1))
                val products = jsonObj.getJSONArray("products")

                for (o in 0..products!!.length() - 1) {
                    val product = products.get(o)
                    productArray.put(product)
                    Log.d("INFO", product.toString())
                }
            }
            Database.getTable(MainActivity.TABLE_PRODUCT)!!.write(productArray.toString())
            return true
        }

        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun onProgressUpdate(vararg progress: Int?) {
            super.onProgressUpdate(*progress)
            //setProgressPercent(progress[0])
        }

        override fun onPostExecute(result: Boolean) {
            super.onPostExecute(result)
            Log.d("INFO", result.toString())
            //asyncInterface!!.processFinish(result)
        }
    }

    private fun downloadProducts() {
        GetProducts().execute()
    }

    class AsyncHttpToJson : AsyncTask<String, Void, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
            Log.d("INFO", "NEW HTTP REQUEST")
        }

        override fun doInBackground(vararg p0: String?): String? {
            Log.d("INFO", "BACKGROUND DOING")
            val req = p0[0]
            val json = URL(req).readText()
            return json
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            Log.d("INFO", result)
            //asyncInterface!!.processFinish(result)
        }
    }*/

    companion object {
        private const val HTTP_API = "https://world.openfoodfacts.org"
    }
}