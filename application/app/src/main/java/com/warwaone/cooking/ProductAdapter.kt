package com.warwaone.cooking

import android.content.Context
import android.media.Image
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.R.attr.thumbnail
import android.view.View
import kotlinx.android.synthetic.main.activity_add_plate.view.*
import com.warwaone.cooking.ProductAdapter.MyViewHolder
import android.view.LayoutInflater




/**
 * Created by xn1a on 21/02/18.
 */
class ProductAdapter(display: Int) : RecyclerView.Adapter<ProductAdapter.MyViewHolder>(), Filterable {

    private val mContext: Context? = null
    private var mList: List<Product>? = ArrayList()
    private val mListFiltered: List<Product>? = ArrayList()
    private val mDisplay: Int = display
    //private val mListener: ContactsAdapterListener? = null

    override fun getFilter(): Filter {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) = holder.bind(mList!![position])

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
       val layout: Int
       if (mDisplay == DISPLAY_LINE){
           layout = R.layout.item_product_line
       }
        else {
          layout = R.layout.item_product_grid
       }

        return MyViewHolder(
                LayoutInflater.from(parent!!.context)
                        .inflate(layout, parent, false)
        )
    }

    override fun getItemCount() = mList!!.size

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Product) = with(itemView) {
            itemView.name.setText(item.name)
            setOnClickListener {

            }
        }
    }

    fun setList (list: List<Product>) {
        mList = list
        notifyDataSetChanged()
    }

    data class Product (val id: Int, val name: String, val image: Image)

    companion object {
        val DISPLAY_LINE = 0
        val DISPLAY_GRID = 1
    }
}